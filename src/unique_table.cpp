#include <stream9/container/unique_table.hpp>

#include <stream9/json.hpp>
#include <stream9/string.hpp>
#include <stream9/strings/stream.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace json = stream9::json;

using stream9::string;
using stream9::unique_table;
using stream9::unique_table_traits;

BOOST_AUTO_TEST_SUITE(unique_table_)

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        unique_table<int, int> t1;
    }

    struct t100 : unique_table_traits {
        static constexpr int min_capacity = 100;
    };
    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        unique_table<int, int, t100> t1;
    }

    BOOST_AUTO_TEST_CASE(constructor_ilist_)
    {
        unique_table<int, int> t1 {
            { 1, 2 },
            { 3, 4 },
        };

        BOOST_TEST(t1.size() == 2);
    }

    BOOST_AUTO_TEST_CASE(bracket_1_)
    {
        unique_table<string, int> t1 {
            { "foo", 4 },
        };

        auto& v1 = t1["foo"];
        BOOST_TEST(v1 == 4);

        auto& v2 = t1["bar"];
        BOOST_TEST(v2 == int());
    }

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        unique_table<string, int> t1 {
            { "foo", 4 },
            { "bar", 2 },
        };

        auto i1 = t1.find("bar");

        BOOST_REQUIRE(i1 != t1.end());
        BOOST_TEST(i1->value == 2);
    }

    BOOST_AUTO_TEST_CASE(find_2_)
    {
        unique_table<string, int> const t1 {
            { "foo", 4 },
            { "bar", 2 },
        };

        auto i1 = t1.find("foo");

        BOOST_REQUIRE(i1 != t1.end());
        BOOST_TEST(i1->value == 4);
    }

    BOOST_AUTO_TEST_CASE(insert_1_)
    {
        unique_table<string, int> t1;

        t1.insert({ "foo", 1 });
        t1.insert({ "bar", 2 });

        json::object expected {
            { "bar", 2 },
            { "foo", 1 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_1_)
    {
        unique_table<string, int> t1;

        t1.emplace("foo", 1);
        t1.emplace("bar", 2);

        json::object expected {
            { "bar", 2 },
            { "foo", 1 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_or_assign_1_)
    {
        unique_table<string, int> t1;

        t1.emplace_or_assign("foo", 1);
        t1.emplace_or_assign("foo", 2);

        json::object expected {
            { "foo", 2 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_with_key_)
    {
        unique_table<string, int> t1 {
            { "foo", 1 },
            { "bar", 2 },
        };

        auto n = t1.erase("foo");

        BOOST_TEST(n == 1);

        json::object expected {
            { "bar", 2 },
        };
        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(ostream_)
    {
        using stream9::strings::operator<<;

        unique_table<string, int> t1 {
            { "foo", 1 },
            { "bar", 2 },
        };

        string s;
        s << t1;

        auto v = json::parse(s);

        json::object expected {
            { "bar", 2 },
            { "foo", 1 },
        };

        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_hint_)
    {
        unique_table<int, int> t;

        t.emplace(0, 1);
        t.emplace(2, 1);
        t.emplace(4, 1);

        {
            json::array expected {
                json::array { 0, 1 },
                json::array { 2, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin(), 3, 1);
            json::array expected {
                json::array { 0, 1 },
                json::array { 2, 1 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 1, 3, 1);
            json::array expected {
                json::array { 0, 1 },
                json::array { 2, 1 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 2, 3, 1);
            json::array expected {
                json::array { 0, 1 },
                json::array { 2, 1 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 3, 3, 1);
            json::array expected {
                json::array { 0, 1 },
                json::array { 2, 1 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        unique_table<int, int> t;
        t.emplace(1, 2);

        auto& [f, l] = t;

        BOOST_TEST(f != l);
        BOOST_TEST(f == t.begin());
        BOOST_TEST(l == t.end());
    }

BOOST_AUTO_TEST_SUITE_END() // unique_table_

} // namespace testing
