#include <stream9/sorted_array.hpp>

#include "namespace.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/node.hpp>

namespace testing {

using stream9::sorted_array;
using std::string;

BOOST_AUTO_TEST_SUITE(sorted_array_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        sorted_array<string> a;
    }

    BOOST_AUTO_TEST_CASE(value_constructor_)
    {
        sorted_array<string> a { "foo" };

        json::array expected { "foo" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(range_constructor_)
    {
        sorted_array<string> a { "foo", "bar", "baz", "bar" };

        json::array expected { "bar", "bar", "baz", "foo" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(copy_insert_value_)
    {
        sorted_array<string> a;

        string s1 = "foo";
        string s2 = "bar";
        string s3 = "baz";

        a.insert(s1);
        json::array e1 { "foo" };
        BOOST_TEST(json::value_from(a) == e1);

        a.insert(s2);
        json::array e2 { "bar", "foo" };
        BOOST_TEST(json::value_from(a) == e2);

        a.insert(s3);
        json::array e3 { "bar", "baz", "foo" };
        BOOST_TEST(json::value_from(a) == e3);
    }

    struct deref
    {
        decltype(auto)
        operator()(auto&& v) const noexcept
        {
            return *v;
        }
    };

    BOOST_AUTO_TEST_CASE(copy_insert_value_stability_)
    {
        using stream9::node;

        sorted_array<node<string>, std::less<>, deref> a;

        node<string> e1 { "foo" };
        node<string> e2 { "foo" };

        auto p1 = &e1;
        auto p2 = &e2;

        BOOST_CHECK(&e1 != &e2);

        a.insert(std::move(e1));
        a.insert(std::move(e2));

        json::array e3 { "foo", "foo" };
        BOOST_TEST(json::value_from(a) == e3);

        BOOST_CHECK(&a[0] == p1);
        BOOST_CHECK(&a[1] == p2);
    }

    BOOST_AUTO_TEST_CASE(move_insert_value_)
    {
        sorted_array<int> a;

        a.insert(100);
        json::array e1 { 100 };
        BOOST_TEST(json::value_from(a) == e1);

        a.insert(95);
        json::array e2 { 95, 100 };
        BOOST_TEST(json::value_from(a) == e2);

        a.insert(99);
        json::array e3 { 95, 99, 100 };
        BOOST_TEST(json::value_from(a) == e3);

        a.insert(96);
        json::array e4 { 95, 96, 99, 100 };
        BOOST_TEST(json::value_from(a) == e4);

        a.insert(95);
        json::array e5 { 95, 95, 96, 99, 100 };
        BOOST_TEST(json::value_from(a) == e5);

        a.insert(99);
        json::array e6 { 95, 95, 96, 99, 99, 100 };
        BOOST_TEST(json::value_from(a) == e6);

        a.insert(100);
        json::array e7 { 95, 95, 96, 99, 99, 100, 100 };
        BOOST_TEST(json::value_from(a) == e7);
    }

    BOOST_AUTO_TEST_CASE(copy_insert_range_)
    {
        using stream9::array;

        sorted_array<int> a { 10, 9, 8, 7, 6 };

        array<int> v { 5, 4, 3, 2, 1, 0 };
        a.insert(v);

        json::array ex { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BOOST_TEST(json::value_from(a) == ex);
    }

    BOOST_AUTO_TEST_CASE(insert_mix_)
    {
        using stream9::array;

        sorted_array<int> a { 8, 7, 6 };

        array<int> v { 5, 4, 3 };
        a.insert(10, 9, v, 2, 1, 0);

        json::array ex { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BOOST_TEST(json::value_from(a) == ex);
    }

    BOOST_AUTO_TEST_CASE(emplace_)
    {
        sorted_array<int> a { 0, 5, 10 };

        a.emplace(3);
        json::array ex1 { 0, 3, 5, 10 };
        BOOST_TEST(json::value_from(a) == ex1);

        a.emplace(7);
        json::array ex2 { 0, 3, 5, 7, 10 };
        BOOST_TEST(json::value_from(a) == ex2);
    }

    BOOST_AUTO_TEST_CASE(resize_)
    {
        sorted_array<int> a { -10, -5, 5, 10 };

        a.resize(6);
        json::array ex1 { -10, -5, 0, 0, 5, 10 };
        BOOST_TEST(json::value_from(a) == ex1);
    }

    BOOST_AUTO_TEST_CASE(lower_bound_)
    {
        using stream9::less;
        using stream9::project;

        struct value { string name; int v; };

        sorted_array<value, less, project<&value::v>> a;
        a.reserve(5);
        a.emplace("v1", 1);
        a.emplace("v2", 2);
        a.emplace("v3", 3);
        auto i4 = a.emplace("v4", 4);
        a.emplace("v5", 5);

        auto i1 = a.lower_bound(3);
        BOOST_CHECK(i1 == a.begin() + 2);

        auto i2 = a.lower_bound(*i4);
        BOOST_CHECK(i2 == a.begin() + 3);
    }

    BOOST_AUTO_TEST_CASE(upper_bound_)
    {
        using stream9::less;
        using stream9::project;

        struct value { string name; int v; };

        sorted_array<value, less, project<&value::v>> a;
        a.reserve(5);
        a.emplace("v1", 1);
        a.emplace("v2", 2);
        a.emplace("v3.1", 3);
        a.emplace("v3.2", 3);
        a.emplace("v4", 4);
        a.emplace("v5", 5);

        auto i1 = a.upper_bound(3);
        BOOST_CHECK(i1 == a.begin() + 4);

        auto i2 = a.upper_bound(value("foo",4));
        BOOST_CHECK(i2 == a.begin() + 5);
    }

    BOOST_AUTO_TEST_CASE(equal_range_)
    {
        using stream9::less;
        using stream9::project;

        struct value { string name; int v; };

        sorted_array<value, less, project<&value::v>> a;
        a.reserve(5);
        a.emplace("v1", 1);
        a.emplace("v2", 2);
        a.emplace("v3.1", 3);
        a.emplace("v3.2", 3);
        a.emplace("v4", 4);
        a.emplace("v5", 5);

        auto [i1, i2] = a.equal_range(3);
        BOOST_CHECK(i1 == a.begin() + 2);
        BOOST_CHECK(i2 == a.begin() + 4);

        auto [i3, i4] = a.equal_range(value("foo",4));
        BOOST_CHECK(i3 == a.begin() + 4);
        BOOST_CHECK(i4 == a.begin() + 5);
    }

BOOST_AUTO_TEST_SUITE_END() // sorted_array_

} // namespace testing
