#include <stream9/container/modifier/binary_insert.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(binary_insert_)


    BOOST_AUTO_TEST_CASE(step_1_)
    {
        std::vector<int> v { 1, 3, 5, 7, 9 };

        auto o_i = con::binary_insert(v, 6);

        BOOST_REQUIRE(o_i);
        BOOST_TEST(std::distance(v.begin(), *o_i) == 3);

        json::array expected { 1, 3, 5, 6, 7, 9 };

        BOOST_TEST(json::value_from(v) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // binary_insert_

} // namespace testing
