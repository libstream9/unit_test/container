#include <stream9/erase_prefix.hpp>

#include "../namespace.hpp"

#include <stream9/array.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::erase_prefix;
using st9::array;

BOOST_AUTO_TEST_SUITE(erase_prefix_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        array<int> a {
            1, 2, 3, 4, 5
        };

        auto i = erase_prefix(a, 0);

        BOOST_CHECK(i == a.begin());

        json::array expected { 1, 2, 3, 4, 5 };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        array<int> a {
            1, 2, 3, 4, 5
        };

        auto i = erase_prefix(a, 2);

        BOOST_CHECK(i == a.begin());

        json::array expected { 3, 4, 5 };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_3_)
    {
        array<int> a {
            1, 2, 3, 4, 5
        };

        auto i = erase_prefix(a, 6);

        BOOST_CHECK(i == a.begin());

        json::array expected { };

        BOOST_TEST(json::value_from(a) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // erase_prefix_

} // namespace testing
