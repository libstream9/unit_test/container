#include <stream9/container/modifier/insert.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <vector>

#include <stream9/array.hpp>
#include <stream9/json.hpp>

namespace testing {

using stream9::container::insert;
using stream9::array;
using std::vector;

BOOST_AUTO_TEST_SUITE(insert_)

    BOOST_AUTO_TEST_CASE(value_insertable_)
    {
        array<int> s1;

        auto i = insert(s1, s1.end(), 1);

        json::array expected { 1, };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((i == s1.begin()));
    }

    BOOST_AUTO_TEST_CASE(range_insertable_)
    {
        array<int> s1 { 0 };
        vector<int> s2 { 1, 2, 3 };

        auto [f, l] = insert(s1, s1.end(), s2);

        json::array expected { 0, 1, 2, 3 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 1);
        BOOST_TEST((l - s1.begin()) == 4);
    }

    //TODO range_insertable_by_iterator

    BOOST_AUTO_TEST_CASE(range_insertable_by_legacy_iterator_)
    {
        vector<int> s1 { 0 };
        array<int> s2 { 1, 2, 3 };

        auto [f, l] = insert(s1, s1.begin(), s2);

        json::array expected { 1, 2, 3, 0 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 0);
        BOOST_TEST((l - s1.begin()) == 3);
    }

    BOOST_AUTO_TEST_CASE(iter_insertable_)
    {
        array<int> s1 { 0 };
        vector<int> s2 { 1, 2, 3 };

        auto [f, l] = insert(s1, s1.end(), s2.begin(), s2.end());

        json::array expected { 0, 1, 2, 3 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 1);
        BOOST_TEST((l - s1.begin()) == 4);
    }

    BOOST_AUTO_TEST_CASE(legacy_iter_insertable_)
    {
        vector<int> s1 { 0 };
        array<int> s2 { 1, 2, 3 };

        auto [f, l] = insert(s1, s1.end(), s2.begin(), s2.end());

        json::array expected { 0, 1, 2, 3 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 1);
        BOOST_TEST((l - s1.begin()) == 4);
    }

    BOOST_AUTO_TEST_CASE(variadic_insertable_1_)
    {
        array<int> s1 { 0 };

        auto [f, l] = insert(s1, s1.end(), 1, 2, 3);

        json::array expected { 0, 1, 2, 3 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 1);
        BOOST_TEST((l - s1.begin()) == 4);
    }

    BOOST_AUTO_TEST_CASE(variadic_insertable_2_)
    {
        array<int> s1 { 0 };
        vector<int> s2 { 2 };

        auto [f, l] = insert(s1, s1.begin(), 1, s2, 3);

        json::array expected { 1, 2, 3, 0 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 0);
        BOOST_TEST((l - s1.begin()) == 3);
    }

    BOOST_AUTO_TEST_CASE(variadic_insertable_legacy_1_)
    {
        vector<int> s1 { 0 };

        auto [f, l] = insert(s1, s1.end(), 1, 2, 3);

        json::array expected { 0, 1, 2, 3 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 1);
        BOOST_TEST((l - s1.begin()) == 4);
    }

    BOOST_AUTO_TEST_CASE(variadic_insertable_legacy_2_)
    {
        vector<int> s1 { 0 };
        array<int> s2 { 2 };

        auto [f, l] = insert(s1, s1.begin(), 1, s2, 3);

        json::array expected { 1, 2, 3, 0 };
        BOOST_TEST(json::value_from(s1) == expected);

        BOOST_TEST((f - s1.begin()) == 0);
        BOOST_TEST((l - s1.begin()) == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // insert_

} // namespace testing
