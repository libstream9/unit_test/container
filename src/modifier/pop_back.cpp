#include <stream9/container/modifier/pop_back.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>

namespace testing {

using stream9::pop_back;
using stream9::array;

BOOST_AUTO_TEST_SUITE(pop_back_)

    BOOST_AUTO_TEST_CASE(array_)
    {
        array<std::string> v1 {
            "foo", "bar", "baz"
        };

        auto v = pop_back(v1);

        array<std::string> expected {
            "foo", "bar",
        };

        BOOST_TEST(v1 == expected);
        BOOST_TEST(v == "baz");
    }

BOOST_AUTO_TEST_SUITE_END() // pop_back_

} // namespace testing
