#include <stream9/container/modifier/erase.hpp>

#include <ranges>
#include <set>
#include <vector>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/iterator_at.hpp>

namespace testing {

using st9::erase;

static boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(erase_)

    BOOST_AUTO_TEST_SUITE(sequence_container_)

        BOOST_AUTO_TEST_CASE(front_)
        {
            std::vector c { 1, 2, 3, 4, 5 };
            auto r = rng::views::take(c, 2);

            auto it = erase(c, r);

            BOOST_CHECK(it == c.begin());

            auto const expected = { 3, 4, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(back_)
        {
            std::vector c { 1, 2, 3, 4, 5 };
            auto r = rng::views::drop(c, 3);

            auto it = erase(c, r);

            BOOST_CHECK(it == c.end());

            auto const expected = { 1, 2, 3 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(middle_)
        {
            std::vector c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(
                rng::iterator_at(c, 1),
                rng::iterator_at(c, 4) );

            auto it = erase(c, r);

            BOOST_CHECK(it == rng::iterator_at(c, 1));

            auto const expected = { 1, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            std::vector c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(c.begin(), c.begin());

            auto it = erase(c, r);

            BOOST_CHECK(it == c.begin());

            auto const expected = { 1, 2, 3, 4, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            std::vector<int> c {};
            auto r = rng::subrange(c.end(), c.end());

            auto it = erase(c, r);

            BOOST_CHECK(it == c.end());

            BOOST_TEST(c.empty());
        }

        BOOST_AUTO_TEST_CASE(all_)
        {
            std::vector c { 1, 2, 3, 4, 5 };

            auto it = erase(c, c);

            BOOST_CHECK(it == c.end());

            BOOST_TEST(c.empty());
        }

        BOOST_AUTO_TEST_CASE(single_)
        {
            std::vector c { 1, 2, 3, 4, 5 };

            auto it = erase(c, c.begin() + 2);

            BOOST_CHECK(it == c.begin() + 2);

            std::vector expected { 1, 2, 4, 5 };
            BOOST_CHECK(c == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // sequence_container_

    BOOST_AUTO_TEST_SUITE(associative_container_)

        BOOST_AUTO_TEST_CASE(front_)
        {
            std::set<int> c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(c.begin(), std::next(c.begin(), 2));

            auto it = erase(c, r);

            BOOST_CHECK(it == c.begin());

            auto const expected = { 3, 4, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(back_)
        {
            std::set<int> c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(std::next(c.begin(), 3), c.end());

            auto it = erase(c, r);

            BOOST_CHECK(it == c.end());

            auto const expected = { 1, 2, 3 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(middle_)
        {
            std::set<int> c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(
                std::next(c.begin(), 1), std::next(c.begin(), 4));

            auto it = erase(c, r);

            BOOST_CHECK(it == std::next(c.begin(), 1));

            auto const expected = { 1, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(all_)
        {
            std::set<int> c { 1, 2, 3, 4, 5 };

            auto it = erase(c, c);

            BOOST_CHECK(it == c.end());

            BOOST_TEST(c.empty());
        }

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            std::set<int> c { 1, 2, 3, 4, 5 };
            auto r = rng::subrange(c.begin(), c.begin());

            auto it = erase(c, r);

            BOOST_CHECK(it == c.begin());

            auto const expected = { 1, 2, 3, 4, 5 };
            BOOST_TEST(c == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            std::set<int> c {};
            auto r = rng::subrange(c.end(), c.end());

            auto it = erase(c, r);

            BOOST_CHECK(it == c.end());

            BOOST_TEST(c.empty());
        }

    BOOST_AUTO_TEST_SUITE_END() // associative_container_

BOOST_AUTO_TEST_SUITE_END() // erase_

} // namespace testing
