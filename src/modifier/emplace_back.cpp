#include <stream9/container/modifier/emplace_back.hpp>

#include "../value.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/container/array.hpp>
#include <stream9/json.hpp>

namespace testing {

using stream9::array;
using stream9::emplace_back;

namespace json { using namespace stream9::json; }

BOOST_AUTO_TEST_SUITE(emplace_back_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        array<value> a;

        emplace_back(a, "1");

        json::array expected { "1" };

        BOOST_TEST(a == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // emplace_back_

} // namespace testing
