#include <stream9/container/modifier/erase_if.hpp>

#include "../namespace.hpp"

#include <stream9/range.hpp>
#include <stream9/unique_table.hpp>

#include <vector>
#include <set>

#include <boost/container/small_vector.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/test/unit_test.hpp>

namespace testing {

using st9::erase_if;

static boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(erase_if_)

    BOOST_AUTO_TEST_CASE(vector_)
    {
        std::vector v { 1, 2, 3, 4, 5 };

        auto const n = erase_if(v, [](auto i) { return i > 3; });

        BOOST_TEST(n == 2);

        auto const expected = { 1, 2, 3 };
        BOOST_TEST(v == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(small_vector_)
    {
        boost::container::small_vector<int, 10> v { 1, 2, 3, 4, 5 };

        auto const n = erase_if(v, [](auto i) { return i > 3; });

        BOOST_TEST(n == 2);

        auto const expected = { 1, 2, 3 };
        BOOST_TEST(v == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s = "12345";

        auto const n = erase_if(s,
            [](auto c) { return (c - '0') % 2 == 0; } );

        BOOST_TEST(n == 2);

        BOOST_TEST(s == "135", per_element);
    }

    BOOST_AUTO_TEST_CASE(set_)
    {
        std::set v { 1, 2, 3, 4, 5 };

        auto const n = erase_if(v, [](auto i) { return i > 3; });

        BOOST_TEST(n == 2);

        auto const expected = { 1, 2, 3 };
        BOOST_TEST(v == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(flat_set_)
    {
        boost::container::flat_set v { 1, 2, 3, 4, 5 };

        auto const n = erase_if(v, [](auto i) { return i % 2 == 1; });

        BOOST_TEST(n == 3);

        auto const expected = { 2, 4 };
        BOOST_TEST(v == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(unique_table_)
    {
        st9::unique_table<int, int> v;
        v.emplace(1, 1);
        v.emplace(2, 1);
        v.emplace(3, 1);

        auto n = erase_if(v, [](auto& e) { return e.key % 2 == 1; });

        BOOST_TEST(n == 2);
        BOOST_TEST(v.begin()->key == 2);
    }

BOOST_AUTO_TEST_SUITE_END() // erase_if_

} // namespace testing
