#include <stream9/append.hpp>

#include "../value.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/container/array.hpp>

namespace testing {

using stream9::array;
using stream9::append;

BOOST_AUTO_TEST_SUITE(append_)

    BOOST_AUTO_TEST_CASE(iterator_range_)
    {
        using T = array<value>;

        T v1;
        auto v2 = { "a", "b", "c" };

        append(v1, v2.begin(), v2.end());

        BOOST_CHECK(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(range_)
    {
        using T = array<value>;

        T v1 { "1", "2", "3" };
        auto v2 = { "a", "b", "c" };

        append(v1, v2);

        auto expected = { "1", "2", "3", "a", "b", "c" };
        BOOST_TEST(v1 == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // append_

} // namespace testing
