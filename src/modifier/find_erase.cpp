#include <stream9/find_erase.hpp>

#include "../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/string.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::array;
using st9::string;
using st9::find_erase;

BOOST_AUTO_TEST_SUITE(find_erase_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        array<string> arr;
        arr.insert("foo");
        arr.insert("bar");
        arr.insert("baz");

        auto rv = find_erase(arr, "bar");
        BOOST_TEST(rv);

        rv = find_erase(arr, "xyzzy");
        BOOST_TEST(!rv);

        json::array expected { "foo", "baz" };
        BOOST_TEST(json::value_from(arr) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_

} // namespace testing
