#include <stream9/expand.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>

namespace testing {

using stream9::expand;
using stream9::array;

BOOST_AUTO_TEST_SUITE(expand_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        array<int> v;
        BOOST_TEST(v.capacity() == 0);

        expand(v, 10);

        BOOST_TEST(v.capacity() == 10);
    }

BOOST_AUTO_TEST_SUITE_END() // expand_

} // namespace testing
