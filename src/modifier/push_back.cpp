#include <stream9/container/modifier/push_back.hpp>

#include "../value.hpp"

#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/container/array.hpp>

namespace testing {

using stream9::array;
using stream9::push_back;

BOOST_AUTO_TEST_SUITE(push_back_)

    BOOST_AUTO_TEST_CASE(lvalue_)
    {
        array<value> v;

        value v1 = "foo";
        push_back(v, v1);

        BOOST_REQUIRE(v.size() == 1);
        BOOST_TEST(v[0] == "foo");
        BOOST_TEST(v1 == "foo");
        BOOST_CHECK(v1.copied);
    }

    BOOST_AUTO_TEST_CASE(rvalue_)
    {
        array<value> v;

        value v1 = "foo";
        push_back(v, std::move(v1));

        BOOST_REQUIRE(v.size() == 1);
        BOOST_TEST(v[0] == "foo");
        BOOST_CHECK(v1.moved);
    }

BOOST_AUTO_TEST_SUITE_END() // push_back_

} // namespace testing
