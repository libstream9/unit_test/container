#include <stream9/container/array.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <filesystem>
#include <string>

#include <stream9/json.hpp>

#include <stream9/node.hpp>

namespace testing {

using std::string;

template<typename T>
using array = stream9::array<T>;
//using array = stream9::array<T, 100>;

BOOST_AUTO_TEST_SUITE(array_)

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using T = array<int>;

        static_assert(std::is_nothrow_default_constructible_v<T>);
        static_assert(std::is_nothrow_move_constructible_v<T>);
        static_assert(std::is_nothrow_move_assignable_v<T>);
        static_assert(std::is_nothrow_destructible_v<T>);
        static_assert(std::is_nothrow_swappable_v<T>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        array<int> a;

        BOOST_TEST(a.size() == 0);
        BOOST_TEST(a.capacity() == a.min_capacity());
    }

    BOOST_AUTO_TEST_CASE(constructor_ilist_1_)
    {
        array<int> a { 1, 2, 3, 4, 5 };

        json::array expected { 1, 2, 3, 4, 5 };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(constructor_ilist_2_)
    {
        array<array<int>> a {
            { 1, 2 },
            { 3, 4 },
        };

        json::array expected {
            { 1, 2 },
            { 3, 4 },
        };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(constructor_single_)
    {
        array<int> a1 { 1 };

        json::array expected { 1, };

        BOOST_TEST(json::value_from(a1) == expected);
    }

    BOOST_AUTO_TEST_CASE(constructor_range_)
    {
        std::vector<int> a1 { 1, 2, 3, 4, 5 };
        array<int> a2 { a1 };

        json::array expected { 1, 2, 3, 4, 5 };

        BOOST_TEST(json::value_from(a2) == expected);
    }

    BOOST_AUTO_TEST_CASE(constructor_variadic_)
    {
        std::vector<int> a1 { 1, 2, 3, 4, 5 };
        array<int> a2 { 0, a1, 6 };

        json::array expected { 0, 1, 2, 3, 4, 5, 6 };
        BOOST_TEST(json::value_from(a2) == expected);
    }

    void
    fill(auto& a, auto n, auto& v)
    {
        for (auto i = 0; i != n; ++i) {
            a.insert(a.end(), v);
        }
    }

    bool
    is_filled(auto& a, auto& v)
    {
        return std::all_of(a.begin(), a.end(), [&](auto& x) { return x == v; });
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_1_)
    {
        array<string> a1;
        fill(a1, a1.capacity(), "X");
        array<string> a2 { a1 };

        BOOST_TEST(a1 == a2);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_2_)
    {
        array<string> a1;
        fill(a1, a1.min_capacity() + 10, "X");
        array<string> a2 { a1 };

        BOOST_TEST(a1 == a2);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_1_)
    {
        array<string> a1;
        fill(a1, a1.capacity(), "X");

        array<string> a2;
        fill(a2, a2.capacity(), "Y");

        a2 = a1;

        BOOST_TEST(a1 == a2);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_2_)
    {
        array<string> a1;
        fill(a1, a1.capacity() + 10, "X");

        array<string> a2;
        fill(a2, a2.capacity() + 10, "Y");

        a2 = a1;

        BOOST_TEST(a1 == a2);
    }

    BOOST_AUTO_TEST_CASE(move_constructor_1_)
    {
        array<string> a1;
        fill(a1, a1.capacity(), "X");
        array<string> a2 { std::move(a1) };

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity());
    }

    BOOST_AUTO_TEST_CASE(move_constructor_2_)
    {
        array<string> a1;
        fill(a1, a1.capacity() + 10, "X");
        array<string> a2 { std::move(a1) };

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity() + 10);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(move_assignment_1_)
    {
        array<string> a1;
        fill(a1, a1.capacity(), "X");

        array<string> a2;
        fill(a2, a2.capacity(), "Y");

        a2 = std::move(a1);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity());
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(move_assignment_2_)
    {
        array<string> a1;
        fill(a1, a1.capacity() + 10, "X");

        array<string> a2;
        fill(a2, a2.capacity(), "Y");

        a2 = std::move(a1);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity() + 10);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(move_assignment_3_)
    {
        array<string> a1;
        fill(a1, a1.capacity(), "X");

        array<string> a2;
        fill(a2, a2.capacity() + 10, "Y");

        a2 = std::move(a1);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity());
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(move_assignment_4_)
    {
        array<string> a1;
        fill(a1, a1.capacity() + 10, "X");

        array<string> a2;
        fill(a2, a2.capacity() + 10, "Y");

        a2 = std::move(a1);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == a1.capacity() + 10);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(insert_single_natural_)
    {
        array<int> a;

        a.insert(100);
        a.insert(200);

        json::array expected { 100, 200 };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_multiple_natural_)
    {
        array<int> a;

        a.insert(100, 200);
        a.insert(200, 300);

        json::array expected { 100, 200, 200, 300 };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_int_1_)
    {
        array<int> a;

        auto i = a.insert(a.end(), 100);
        BOOST_TEST(*i == 100);

        auto r = json::value_from(a);

        json::array expected { 100 };
        BOOST_TEST(r == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_int_2_)
    {
        array<int> a;

        for (auto i = 0; i < 100; ++i) {
            auto j = a.insert(a.end(), i);
            BOOST_TEST(*j == i);
        }

        auto r = json::value_from(a);

        json::array expected;
        for (auto i = 0; i < 100; ++i) {
            expected.push_back(i);
        }

        BOOST_TEST(r == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_string_1_)
    {
        array<std::string> a;

        auto i1 = a.insert(a.end(), "a");
        BOOST_TEST(*i1 == "a");
        auto i2 = a.insert(a.end(), "b");
        BOOST_TEST(*i2 == "b");
        auto i3 = a.insert(a.end(), "c");
        BOOST_TEST(*i3 == "c");
        auto i4 = a.insert(a.end(), "d");
        BOOST_TEST(*i4 == "d");

        json::array expected { "a", "b", "c", "d" };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_string_2_)
    {
        array<std::string> a;

        a.insert(a.end(), "a");
        a.insert(a.end(), "b");
        a.insert(a.end(), "c");
        a.insert(a.end(), "d");

        std::string e = "e";
        a.insert(a.begin() + 2, std::move(e));

        json::array expected { "a", "b", "e", "c", "d" };

        BOOST_TEST(json::value_from(a) == expected);
        BOOST_TEST(e.empty());
    }

    BOOST_AUTO_TEST_CASE(insert_string_3_)
    {
        array<string> a { "a", "b", };

        a.insert(a.begin() + 1, "x");

        json::array expected { "a", "x", "b" };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_range_1_)
    {
        array<int> a;

        auto v = { 1, 2, 3, 4, 5 };

        auto r1 = a.insert(a.end(), v.begin(), v.end());
        BOOST_TEST(r1.size() == 5);
        BOOST_TEST(*r1.begin() == 1);
        {
            json::array expected { 1, 2, 3, 4, 5 };
            BOOST_TEST(json::value_from(a) == expected);
        }

        auto r2 = a.insert(a.begin() + 2, v.begin(), v.end());
        BOOST_TEST(r2.size() == 5);
        BOOST_TEST(*r2.begin() == 1);
        {
            json::array expected { 1, 2, 1, 2, 3, 4, 5, 3, 4, 5 };
            BOOST_TEST(json::value_from(a) == expected);
        }

    }

    BOOST_AUTO_TEST_CASE(insert_range_2_)
    {
        array<int> a;
        a.reserve(100);

        auto v = { 1, 2, 3, 4, 5 };

        auto r1 = a.insert(a.end(), v.begin(), v.end());
        BOOST_TEST(r1.size() == 5);
        BOOST_TEST(*r1.begin() == 1);
        {
            json::array expected { 1, 2, 3, 4, 5 };
            BOOST_TEST(json::value_from(a) == expected);
        }

        auto r2 = a.insert(a.begin() + 2, v.begin(), v.end());
        BOOST_TEST(r2.size() == 5);
        BOOST_TEST(*r2.begin() == 1);
        {
            json::array expected { 1, 2, 1, 2, 3, 4, 5, 3, 4, 5 };
            BOOST_TEST(json::value_from(a) == expected);
        }
    }

    BOOST_AUTO_TEST_CASE(insert_range_3_)
    {
        array<string> a;

        auto v = { "1", "2", "3", "4", "5" };

        auto r1 = a.insert(a.end(), v.begin(), v.end());
        BOOST_TEST(r1.size() == 5);
        BOOST_TEST(*r1.begin() == "1");
        {
            json::array expected { "1", "2", "3", "4", "5" };
            BOOST_TEST(json::value_from(a) == expected);
        }

        auto r2 = a.insert(a.begin() + 2, v.begin(), v.end());
        BOOST_TEST(r2.size() == 5);
        BOOST_TEST(*r2.begin() == "1");
        BOOST_TEST(*(r2.end()-1) == "5");
        {
            json::array expected { "1", "2", "1", "2", "3", "4", "5", "3", "4", "5" };
            BOOST_TEST(json::value_from(a) == expected);
        }
    }

    BOOST_AUTO_TEST_CASE(insert_variadic_)
    {
        std::vector<int> a1 { 1, 2, 3, 4, 5 };
        array<int> a2;

        auto [i1, i2] = a2.insert(a2.end(), 0, a1, 6);

        json::array expected { 0, 1, 2, 3, 4, 5, 6 };
        BOOST_TEST(json::value_from(a2) == expected);

        BOOST_CHECK(i1 == a2.begin());
        BOOST_CHECK(i2 == a2.end());
    }

    BOOST_AUTO_TEST_CASE(insert_ambiguous_1_)
    {
        // fs::path can be constructed from fs::path but also it can be constructed
        // from range_reference_t<fs::path>
        using std::filesystem::path;

        array<path> a;

        path p1 { "/foo/bar" };

        a.insert(a.end(), p1);

        json::array expected { "/foo/bar" }; // not { "foo", "bar" }

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_ambiguous_2_)
    {
        // fs::path can be constructed from fs::path but also it can be constructed
        // from range_reference_t<fs::path>
        using std::filesystem::path;

        array<path> a;

        path p1 { "/foo/bar" };

        a.insert(a.end(), p1, p1);

        json::array expected { "/foo/bar", "/foo/bar" };

        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(clear_1_)
    {
        array<string> a;

        auto c = a.capacity();

        a.clear();
        BOOST_TEST(a.capacity() == c);

        json::array expected {};
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(clear_2_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto c = a.capacity();

        a.clear();
        BOOST_TEST(a.capacity() == c);

        json::array expected {};
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_1_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.begin(), a.end());
        BOOST_CHECK(i == a.end());

        json::array expected { };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_2_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.begin(), a.begin() + 1);
        BOOST_CHECK(i == a.begin());

        json::array expected { "b", "c", "d", "e" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_3_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.end() - 1, a.end());
        BOOST_CHECK(i == a.end());

        json::array expected { "a", "b", "c", "d" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_4_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.begin() + 1, a.end() - 1);
        BOOST_CHECK(i == a.end() - 1);

        json::array expected { "a", "e" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_5_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.begin(), a.begin());
        BOOST_CHECK(i == a.begin());

        json::array expected { "a", "b", "c", "d", "e" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_6_)
    {
        array<string> a { "a", "b", "c", "d", "e" };

        auto i = a.erase(a.end(), a.end());
        BOOST_CHECK(i == a.end());

        json::array expected { "a", "b", "c", "d", "e" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_7_)
    {
        string s;
        s.resize(256);

        array<string> a { s };

        auto i = a.erase(a.begin());
        BOOST_CHECK(i == a.end());

        BOOST_TEST(a.empty());
    }

    BOOST_AUTO_TEST_CASE(erase_8_)
    {
        string s;
        s.resize(256);

        array<string> a { s };

        auto i = a.erase(a.begin(), a.end());
        BOOST_CHECK(i == a.end());

        BOOST_TEST(a.empty());
    }

    BOOST_AUTO_TEST_CASE(resize_1_)
    {
        array<string> a { "1", "2", "3" };

        a.resize(3);

        json::array expected { "1", "2", "3" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(resize_2_)
    {
        array<string> a { "1", "2", "3" };

        a.resize(2);

        json::array expected { "1", "2" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(resize_3_)
    {
        array<string> a { "1", "2", "3" };

        a.resize(0);

        json::array expected { };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(resize_4_)
    {
        array<string> a { "1", "2", "3" };

        a.resize(5);

        json::array expected { "1", "2", "3", "", "" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(resize_5_)
    {
        array<string> a { "1", "2", "3" };

        a.resize(5, "X");

        json::array expected { "1", "2", "3", "X", "X" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(reserve_1_)
    {
        array<int> a { 1, 2, 3, 4, 5 };

        auto c = a.capacity();

        a.reserve(c * 2);
        BOOST_TEST(a.size() == 5);
        BOOST_TEST(a.capacity() == c * 2);

        json::array expected { 1, 2, 3, 4, 5 };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(shrink_to_fit_1_)
    {
        array<int> a;

        a.reserve(20);

        a.shrink_to_fit();

        BOOST_TEST(a.capacity() == a.min_capacity());
    }

    BOOST_AUTO_TEST_CASE(shrink_to_fit_2_)
    {
        array<string> a;

        a.reserve(a.min_capacity());

        a.insert(a.end(), "1");

        a.shrink_to_fit();

        if (a.min_capacity() > 1) {
            BOOST_TEST(a.capacity() == a.min_capacity());
        }
        else {
            BOOST_TEST(a.capacity() == 1);
        }
        BOOST_TEST(a.size() == 1);

        json::array expected { "1" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(swap_0_)
    {
        using std::swap;
        array<string> a1 { "a", "b", "c" };
        array<string> a2 { "1", "2", "3" };

        swap(a1, a2);

        json::array expected1 { "1", "2", "3" };
        BOOST_TEST(json::value_from(a1) == expected1);

        json::array expected2 { "a", "b", "c" };
        BOOST_TEST(json::value_from(a2) == expected2);
    }

    BOOST_AUTO_TEST_CASE(swap_1_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity(), "X");

        array<string> a2;
        fill(a2, a2.min_capacity(), "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity());
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity());
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_2_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity(), "X");

        array<string> a2;
        fill(a2, a2.min_capacity() / 2, "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity() / 2);
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity());
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_3_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity() / 2, "X");

        array<string> a2;
        fill(a2, a2.min_capacity(), "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity());
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity() / 2);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_4_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity() + 1, "X");

        array<string> a2;
        fill(a2, a2.min_capacity() + 1, "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity() + 1);
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity() + 1);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_5_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity() + 1, "X");

        array<string> a2;
        fill(a2, a2.min_capacity() + 2, "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity() + 2);
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity() + 1);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_6_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity() + 2, "X");

        array<string> a2;
        fill(a2, a2.min_capacity() + 1, "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity() + 1);
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity() + 2);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_7_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity() + 1, "X");

        array<string> a2;
        fill(a2, a2.min_capacity(), "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity());
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity() + 1);
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(swap_8_)
    {
        using std::swap;

        array<string> a1;
        fill(a1, a1.min_capacity(), "X");

        array<string> a2;
        fill(a2, a2.min_capacity() + 1, "Y");

        swap(a1, a2);

        BOOST_TEST(a1.size() == a2.min_capacity() + 1);
        BOOST_TEST(is_filled(a1, "Y"));

        BOOST_TEST(a2.size() == a1.min_capacity());
        BOOST_TEST(is_filled(a2, "X"));
    }

    BOOST_AUTO_TEST_CASE(equality_1_)
    {
        array<int> a1 { 1, 2, 3 };
        array<long> a2 { 1, 2, 3 };
        array<unsigned int> a3 { 1, 2, 3, 4 };
        array<unsigned int> a4 { 1, 2, };
        array<unsigned int> a5 { 1, 2, 4 };

        BOOST_TEST(a1 == a2);
        BOOST_TEST(a3 != a1);
        BOOST_TEST(a1 != a4);
        BOOST_TEST(a5 != a1);
    }

    BOOST_AUTO_TEST_CASE(ordering_1_)
    {
        array<int> a1 { 1, 2, 3 };
        array<long> a2 { 1, 2, 3 };
        array<float> a3 { 1, 2, 3, 4 };
        array<float> a4 { 1, 2, };
        array<float> a5 { 1, 2, 4 };

        BOOST_TEST(a1 == a2);
        BOOST_CHECK(a3 > a1);
        BOOST_CHECK(a1 > a4);
        BOOST_CHECK(a5 > a1);
    }

    BOOST_AUTO_TEST_CASE(incomplete_type_)
    {
        struct foo {
            int x;
            stream9::array<foo> y;
        };

        foo a;
    }

    BOOST_AUTO_TEST_CASE(memory_leak_1_)
    {
        struct bar
        {
            std::string s;

            bar()
            {
                s.resize(512);
            }

            bar(int i)
            {
                s.resize(512);

                if (i == 100) throw "bar";
            }
        };
        struct foo
        {
            stream9::node<bar> b1;
            stream9::node<bar> b2;

            foo(int i)
                : b1 {}
                , b2 { i }
            {}
        };

        stream9::array<foo> a;

        stream9::array<int> v { 100, 1, 1, 100, 1, 100, 1, 100 };

        for (auto const& i: v) {
            try {
                a.emplace(a.end(), i);
            }
            catch (...) {
            }
        }
    }

BOOST_AUTO_TEST_SUITE_END() // array_

} // namespace testing
