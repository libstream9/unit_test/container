#include <stream9/container/sorted_table.hpp>

#include "namespace.hpp"

#include <stream9/json.hpp>
#include <stream9/string.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;
using st9::sorted_table;

BOOST_AUTO_TEST_SUITE(sorted_table_)

    BOOST_AUTO_TEST_CASE(insert_1_)
    {
        sorted_table<string, string> t;

        t.insert({ "foo", "bar" });
        t.insert({ "bar", "foo" });
        t.insert({ "foo", "baz" });

        json::array result;
        for (auto& [k, v]: t) {
            result.push_back(k + ":" + v);
        }

        json::array expected {
            "bar:foo", "foo:bar", "foo:baz",
        };

        BOOST_TEST(result == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_1_)
    {
        sorted_table<string, string> t;

        t.emplace("foo", "bar");
        t.emplace("bar", "foo");
        t.emplace("foo", "baz");

        json::array result;
        for (auto& [k, v]: t) {
            result.push_back(k + ":" + v);
        }

        json::array expected {
            "bar:foo", "foo:bar", "foo:baz",
        };

        BOOST_TEST(result == expected);
    }

    BOOST_AUTO_TEST_CASE(equal_range_1_)
    {
        sorted_table<string, string> t;

        t.emplace("foo", "bar");
        t.emplace("bar", "foo");
        t.emplace("foo", "baz");

        static_assert(noexcept(t.equal_range("baz")));

        auto [f, l] = t.equal_range("foo");
        BOOST_REQUIRE(f != t.end());
        BOOST_TEST(f->value == "bar");
        BOOST_TEST(l == t.end());
    }

    BOOST_AUTO_TEST_CASE(equal_range_2_)
    {
        sorted_table<string, string> t;

        t.emplace("foo", "bar");
        t.emplace("bar", "foo");
        t.emplace("foo", "baz");

        auto const& t2 = t;

        static_assert(noexcept(t2.equal_range("baz")));

        auto [f, l] = t2.equal_range("baz");
        BOOST_REQUIRE(f == l);
    }

    BOOST_AUTO_TEST_CASE(emplace_hint_)
    {
        sorted_table<int, int> t;

        t.emplace(0, 1);
        t.emplace(1, 1);
        t.emplace(2, 1);
        t.emplace(2, 2);
        t.emplace(2, 3);
        t.emplace(3, 1);
        t.emplace(4, 1);

        {
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin(), 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 4 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 1, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 4 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 2, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 4 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 3, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 4 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 4, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 4 },
                json::array { 2, 3 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 5, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 2, 4 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 6, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 2, 4 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
        {
            auto t1 = t;
            t1.emplace_hint(t1.begin() + 7, 2, 4);
            json::array expected {
                json::array { 0, 1 },
                json::array { 1, 1 },
                json::array { 2, 1 },
                json::array { 2, 2 },
                json::array { 2, 3 },
                json::array { 2, 4 },
                json::array { 3, 1 },
                json::array { 4, 1 },
            };
            BOOST_TEST(json::value_from(t1) == expected);
        }
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        sorted_table<int, int> t;

        auto& [f, l] = t;

        BOOST_TEST(f == l);
    }

BOOST_AUTO_TEST_SUITE_END() // sorted_table_

} // namespace testing
