#ifndef STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP
#define STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}
namespace stream9::ranges {}
namespace stream9::ranges::views {}
namespace stream9::container {}
namespace stream9::json {}

namespace testing {

namespace st9 = stream9;
namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }
namespace rng::views { using namespace std::ranges::views; }
namespace rng::views { using namespace stream9::ranges::views; }
namespace con { using namespace stream9::container; }
namespace json { using namespace stream9::json; }

} // namespace testing

#endif // STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP
