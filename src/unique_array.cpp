#include <stream9/unique_array.hpp>

#include "namespace.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/projection.hpp>
#include <stream9/less.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace testing {

using stream9::unique_array;
using stream9::string;
using stream9::string_view;

BOOST_AUTO_TEST_SUITE(unique_array_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        unique_array<string> a;

        BOOST_TEST(a.empty());
    }

    BOOST_AUTO_TEST_CASE(initializer_list_1_)
    {
        unique_array<int> a {
            1, 2, 3, 4, 3
        };

        BOOST_TEST(a.size() == 4);
    }

    BOOST_AUTO_TEST_CASE(vairadic_init_1_)
    {
        unique_array<string> a {
            "foo", string_view("bar"), "bar"
        };

        BOOST_TEST(a.size() == 2);
    }

    BOOST_AUTO_TEST_CASE(copy_insert_)
    {
        unique_array<string> a;
        string s1 = "foo";

        auto i1 = a.insert(s1);
        BOOST_REQUIRE(i1);
        BOOST_TEST(*i1 == s1);
        json::array e1 { "foo" };
        BOOST_TEST(json::value_from(a) == e1);

        auto i2 = a.insert(s1);
        BOOST_REQUIRE(!i2);
        BOOST_CHECK(i1.position == i2.position);

        string s2 = "bar";
        auto i3 = a.insert(s2);
        BOOST_REQUIRE(i3);
        json::array e2 { "bar", "foo" };
        BOOST_TEST(json::value_from(a) == e2);

        string s3 = "baz";
        auto i4 = a.insert(s3);
        BOOST_REQUIRE(i4);
        json::array e3 { "bar", "baz", "foo" };
        BOOST_TEST(json::value_from(a) == e3);
    }

    BOOST_AUTO_TEST_CASE(move_insert_)
    {
        unique_array<string> a;

        auto i1 = a.insert("foo");
        BOOST_REQUIRE(i1);
        BOOST_TEST(*i1 == "foo");
        json::array e1 { "foo" };
        BOOST_TEST(json::value_from(a) == e1);

        auto i2 = a.insert("foo");
        BOOST_REQUIRE(!i2);
        BOOST_CHECK(i1.position == i2.position);

        auto i3 = a.insert("bar");
        BOOST_REQUIRE(i3);
        json::array e2 { "bar", "foo" };
        BOOST_TEST(json::value_from(a) == e2);

        auto i4 = a.insert("baz");
        BOOST_REQUIRE(i4);
        json::array e3 { "bar", "baz", "foo" };
        BOOST_TEST(json::value_from(a) == e3);
    }

    BOOST_AUTO_TEST_CASE(insert_or_replace_)
    {
        unique_array<int> a;

        auto i1 = a.insert_or_replace(100);
        BOOST_REQUIRE(i1);
        BOOST_TEST(*i1 == 100);
        json::array e1 { 100 };
        BOOST_TEST(json::value_from(a) == e1);

        auto i2 = a.insert_or_replace(100);
        BOOST_REQUIRE(!i2);
        BOOST_TEST(*i2 == 100);
        json::array e2 { 100 };
        BOOST_TEST(json::value_from(a) == e2);
    }

    BOOST_AUTO_TEST_CASE(insert_complex_)
    {
        using stream9::project;
        using stream9::less;

        struct foo {
            int x;
            int y;
        };

        unique_array<foo, less, project<&foo::x>> a;

        auto i1 = a.insert({100, 200});
        BOOST_CHECK(i1);

        auto i2 = a.insert({100, 300});
        BOOST_CHECK(!i2);
    }

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        unique_array<string> a;

        a.insert("foo");
        a.insert("bar");
        a.insert("baz");

        auto i1 = a.find("foo");
        BOOST_REQUIRE(i1);
        BOOST_TEST(*i1 == "foo");

        auto i2 = a.find("fo");
        BOOST_REQUIRE(i2 == a.end());

        auto i3 = a.find("fooo");
        BOOST_REQUIRE(i3 == a.end());
    }

    BOOST_AUTO_TEST_CASE(find_bug_fix_1_)
    {
        unique_array<int, stream9::less, std::identity, 10> a;

        a.insert(10);

        auto i1 = a.find(5);
        BOOST_REQUIRE(i1 == a.end());
    }

    BOOST_AUTO_TEST_CASE(projected_find_)
    {
        using stream9::project;
        using stream9::less;

        struct foo {
            string v1;
            int v2;

            string& f1() { return v1; }
            string const& f2() { return v1; }
        };

        unique_array<foo, less, project<&foo::f1>> a;

        a.insert(foo{"foo", 100});
        a.insert(foo{"bar", 500});
        a.insert(foo{"baz", 900});

        BOOST_REQUIRE(a.size() == 3);
        BOOST_TEST(a[0].v1 == "bar");
        BOOST_TEST(a[0].v2 == 500);
        BOOST_TEST(a[1].v1 == "baz");
        BOOST_TEST(a[1].v2 == 900);
        BOOST_TEST(a[2].v1 == "foo");
        BOOST_TEST(a[2].v2 == 100);
    }

    BOOST_AUTO_TEST_CASE(lower_bound_)
    {
        unique_array<int> a;

        a.insert(10);
        a.insert(5);
        a.insert(1);

        auto i1 = a.lower_bound(4);
        BOOST_REQUIRE(i1 != a.end());
        BOOST_TEST(*i1 == 5);
    }

    BOOST_AUTO_TEST_CASE(upper_bound_)
    {
        unique_array<int> a;

        a.insert(10);
        a.insert(5);
        a.insert(1);

        auto i1 = a.upper_bound(4);
        BOOST_REQUIRE(i1 != a.end());
        BOOST_TEST(*i1 == 5);
    }

BOOST_AUTO_TEST_SUITE_END() // unique_array_

} // namespace testing
