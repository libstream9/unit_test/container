#ifndef STREAM9_TEST_CONTAINER_SRC_VALUE_HPP
#define STREAM9_TEST_CONTAINER_SRC_VALUE_HPP

#include <ostream>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

namespace testing {

using std::string;
using std::string_view;

struct value
{
    string v;
    bool default_constructed : 1 = false;
    bool value_constructed : 1 = false;
    bool copy_constructed : 1 = false;
    bool move_constructed : 1 = false;
    bool copy_assigned : 1 = false;
    bool move_assigned : 1 = false;
    bool destructed : 1 = false;
    mutable bool copied : 1 = false;
    bool moved : 1 = false;

    template<typename T>
        requires std::is_convertible_v<T, string>
    value(T&& v)
        : v { std::forward<T>(v) }
        , value_constructed { true }
    {}

    value(value const& o)
        : v { o.v }
        , copy_constructed { true }
    {
        o.copied = true;
    }

    value& operator=(value const& o)
    {
        v = o.v;
        copy_assigned = true;
        o.copied = true;
        return *this;
    }

    value(value&& o) noexcept
        : v { std::move(o.v) }
        , move_constructed { true }
    {
        o.moved = true;
    }

    value& operator=(value&& o) noexcept
    {
        v = std::move(o.v);
        move_assigned = true;
        o.moved = true;
        return *this;
    }

    ~value() noexcept
    {
        destructed = true;
    }

    operator string_view () const noexcept
    {
        return v;
    }

    template<typename T>
    bool operator==(T const& s) const noexcept
        requires std::is_convertible_v<T, string_view>
    {
        return v == s;
    }

    friend std::ostream&
    operator<<(std::ostream& os, value const& v)
    {
        return os << v.v;
    }
};

} // namespace testing

#endif // STREAM9_TEST_CONTAINER_SRC_VALUE_HPP
