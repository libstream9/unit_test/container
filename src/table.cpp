#include <stream9/table.hpp>

#include "namespace.hpp"

#include <stream9/string.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::table;
using st9::string;

BOOST_AUTO_TEST_SUITE(table_)

    BOOST_AUTO_TEST_CASE(concepts_1_)
    {
        using T = table<int, int>;

        static_assert(st9::default_constructible<T>);
        static_assert(st9::copyable<T>);
        static_assert(st9::nothrow_movable<T>);
        static_assert(st9::nothrow_destructible<T>);
    }

    BOOST_AUTO_TEST_CASE(concepts_2_)
    {
        using T = table<string, string>;

        static_assert(st9::default_constructible<T>);
        static_assert(st9::copyable<T>);
        static_assert(st9::nothrow_movable<T>);
        static_assert(st9::nothrow_destructible<T>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        table<int, int> t1;

        BOOST_TEST(t1.empty());
    }

    BOOST_AUTO_TEST_CASE(initializer_list_)
    {
        table<int, int> t1 {
            { 1, 2 },
            { 3, 4 },
        };

        BOOST_TEST(t1.size() == 2);
    }

    BOOST_AUTO_TEST_CASE(begin_end_1_)
    {
        table<int, int> t1 {
            { 1, 2 },
            { 3, 4 },
        };

        auto& [i, e] = t1;
        BOOST_TEST(i->key == 1);
        BOOST_TEST(i->value == 2);

        i += 2;
        BOOST_CHECK(i == e);
    }

    BOOST_AUTO_TEST_CASE(begin_end_2_)
    {
        table<int, int> const t1 {
            { 1, 2 },
            { 3, 4 },
        };

        auto& [i, e] = t1;
        BOOST_TEST(i->key == 1);
        BOOST_TEST(i->value == 2);

        i += 2;
        BOOST_CHECK(i == e);
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        table<int, int> t1 {
            { 1, 2 },
            { 3, 4 },
        };

        t1[2] = 5;

        BOOST_TEST(t1.size() == 3);
        BOOST_TEST(t1[1] == 2);
        BOOST_TEST(t1[2] == 5);
    }

    BOOST_AUTO_TEST_CASE(remove_const_)
    {
        table<int, int> t1;

        decltype(t1)::const_iterator i1 = t1.begin();

        auto i2 = t1.remove_const(i1);

        BOOST_CHECK(i1 == i2);
    }

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        table<int, int> t1 {
            { 1, 2 },
            { 1, 4 },
        };

        auto i = t1.find(1);
        BOOST_REQUIRE(i != t1.end());
        BOOST_TEST(i->value == 2);
    }

    BOOST_AUTO_TEST_CASE(find_2_)
    {
        table<int, int> const t1 {
            { 1, 2 },
            { 1, 4 },
        };

        auto i = t1.find(1);
        BOOST_REQUIRE(i != t1.end());
        BOOST_TEST(i->value == 2);
    }

    BOOST_AUTO_TEST_CASE(insert_1_)
    {
        table<int, int> t1;

        t1.insert({ 1, 2 });
        t1.insert({ 3, 4 });

        json::object expected {
            { "1", 2 }, { "3", 4 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_at_)
    {
        table<int, int> t1;

        t1.insert({ 1, 2 });
        auto i = t1.insert({ 3, 4 });
        t1.insert_at(i, { 5, 6 });

        json::object expected {
            { "1", 2 }, { "5", 6 }, { "3", 4 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_1_)
    {
        table<int, int> t1;

        t1.emplace(1, 2);
        t1.emplace(3, 4);

        json::object expected {
            { "1", 2 }, { "3", 4 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(emplace_at_)
    {
        table<int, int> t1;

        t1.emplace(1, 2);
        auto i = t1.emplace(3, 4);
        t1.emplace_at(i, 5, 6);

        json::object expected {
            { "1", 2 }, { "5", 6 }, { "3", 4 },
        };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_1_)
    {
        table<int, int> t1;

        BOOST_TEST(t1.empty());
        t1.erase(t1.begin());
        BOOST_TEST(t1.empty());

        t1.emplace(1, 2);
        t1.erase(t1.begin());
        BOOST_TEST(t1.empty());
    }

    BOOST_AUTO_TEST_CASE(erase_2_)
    {
        table<int, int> t1;

        BOOST_TEST(t1.empty());
        t1.erase(t1.begin(), t1.end());
        BOOST_TEST(t1.empty());

        t1.emplace(1, 2);
        t1.emplace(2, 3);
        t1.erase(t1.begin(), t1.begin() + 1);

        json::object expected { { "2", 3 } };

        BOOST_TEST(json::value_from(t1) == expected);
    }

    BOOST_AUTO_TEST_CASE(clear_)
    {
        table<int, int> t1 { { 1, 2 }, { 2, 3 } };

        t1.clear();

        BOOST_TEST(t1.empty());
    }

    BOOST_AUTO_TEST_CASE(resize_)
    {
        table<int, int> t1 { { 1, 2 }, { 2, 3 } };

        t1.resize(1);

        json::object expected { { "1", 2 } };
        BOOST_TEST(json::value_from(t1) == expected);

        t1.resize(2);
        json::object expected2 { { "1", 2 }, { "0", 0 } };
        BOOST_TEST(json::value_from(t1) == expected2);
    }

    BOOST_AUTO_TEST_CASE(equality_)
    {
        table<int, int> t1, t2;

        BOOST_TEST(t1 == t2);

        t1.emplace(1, 2);
        BOOST_TEST(t1 != t2);

        t2.emplace(1, 2);
        BOOST_TEST(t1 == t2);

        t1.emplace(2, 3);
        auto i = t2.emplace(3, 4);
        BOOST_TEST(t1 != t2);

        t2.erase(i);
        t2.emplace(2, 3);
        BOOST_TEST(t1 == t2);
    }

BOOST_AUTO_TEST_SUITE_END() // table_

} // namespace testing
