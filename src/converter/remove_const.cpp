#include <stream9/container/converter/remove_const.hpp>

#include "../namespace.hpp"

#include <map>
#include <set>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(remove_const_)

    BOOST_AUTO_TEST_CASE(vector_)
    {
        using C = std::vector<int>;

        C c;
        C::const_iterator i1 = c.begin();

        auto i2 = con::remove_const(c, i1);
        static_assert(std::same_as<decltype(i2), C::iterator>);

        static_assert(!noexcept(con::remove_const(c, i1)));
    }

    BOOST_AUTO_TEST_CASE(set_)
    {
        using C = std::set<int>;

        C c;
        C::const_iterator i1 = c.begin();

        auto i2 = con::remove_const(c, i1);
        static_assert(std::same_as<decltype(i2), C::iterator>);

        static_assert(!noexcept(con::remove_const(c, i1)));
    }

    BOOST_AUTO_TEST_CASE(map_)
    {
        using C = std::map<int, int>;

        C c;
        C::const_iterator i1 = c.begin();

        auto i2 = con::remove_const(c, i1);
        static_assert(std::same_as<decltype(i2), C::iterator>);

        static_assert(!noexcept(con::remove_const(c, i1)));
    }

BOOST_AUTO_TEST_SUITE_END() // remove_const_

} // namespace testing
