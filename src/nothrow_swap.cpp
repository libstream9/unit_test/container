#include <stream9/nothrow_swap.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

using stream9::nothrow_swap;

BOOST_AUTO_TEST_SUITE(swap_)

    BOOST_AUTO_TEST_CASE(int_)
    {
        int i = 100, j = 200;

        nothrow_swap(i, j);

        BOOST_TEST(j == 100);
        BOOST_TEST(i == 200);
    }

    BOOST_AUTO_TEST_CASE(throwing_swap_1_)
    {
        static_assert(!std::is_nothrow_swappable_v<json::value&>);
        static_assert(stream9::is_nothrow_swappable_v<json::value&>);

        json::value v1 = "foo";
        json::value v2 = "bar";

        nothrow_swap(v1, v2);

        BOOST_TEST(v1 == "bar");
        BOOST_TEST(v2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(throwing_swap_2_)
    {
        static_assert(!std::is_nothrow_swappable_v<std::exception_ptr>);
        static_assert(stream9::is_nothrow_swappable_v<std::exception_ptr>);
    }

BOOST_AUTO_TEST_SUITE_END() // swap_

} // namespace testing
